# Base Image with all the stuff
FROM node:12.4.0-alpine AS base
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY . .
RUN yarn && yarn  build

# Production Image
FROM danjellz/http-server:latest
WORKDIR /public
COPY --from=base /usr/src/app/public/ .